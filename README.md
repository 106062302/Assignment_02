# 藍色代表有的功能
# 藍色代表有的功能

Complete game process : 15% 
    None

Basic rules : 20% 有
#   • player : can move, shoot and it’s life will decrease when touch enemy’s bullets 
#   • Enemy : system should generate enemy and each enemy can move and attack 
#   • Map : background will move through the game 

Animations : 10% 
    None

Particle Systems : 10% 有
#   – Add particle systems to player’s and enemy’s bullets 

Sound effects : 5% 有
#   – At least two kinds of sound effect 
    – Can change volume 
#   – Ex : shoot sound effect, bgm …

UI : 5% 有
#   – Player health, Ultimate skill number or power, Score, volume control and Game pause 

Leaderboard : 5% 
    None

 Bonus : 
    None
